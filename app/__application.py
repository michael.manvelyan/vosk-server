import os
import time
import uuid

from app.__transcribe import recognize

from vosk import Model
from flask import Flask, render_template, request, session
from flask_socketio import SocketIO, emit, join_room
from threading import Thread, Event


app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret key!'
socketio = SocketIO(app, logger=True, engineio_logger=True)

UPLOAD_DIR = os.path.join('app', 'static', 'uploads')
model = Model(lang="en-us", model_path=os.path.join('model', 'vosk-model-en-us-0.22'))

thread = Thread()
thread_stop_event = Event()


thread = Thread()
thread_stop_event = Event()


class ProgressThread(Thread):
    def __init__(self):
        self.delay = 1
        self._result = ''
        self.data = []
        self.sleep = 0.01
        self.room = str(uuid.uuid4())

        join_room(self.room)

        super(ProgressThread, self).__init__()

    def test_function(self):
        recognize(model, self.data, self)

    @property
    def result(self):
        return self._result

    @result.setter
    def result(self, value):
        self._result = value
        time.sleep(self.sleep)
        socketio.emit('result', {'result': self._result}, namespace='/progress', room=self.room)

    def run(self):
        self.test_function()


@app.route('/')
def stream():
    return render_template('stream.html')


@socketio.on('stream', namespace='/progress')
def use_stream(data):
    global thread

    print(data)
    thread.data = data
    thread.start()


@socketio.on('connect', namespace='/progress')
def test_connect():
    global thread

    print("Starting ProgressThread")
    thread = ProgressThread()
