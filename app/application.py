import os
import time
import uuid
import audioop

from app.__transcribe import recognize

from vosk import Model, KaldiRecognizer
from flask import Flask, render_template, session
from flask_socketio import SocketIO, emit, join_room, leave_room, close_room, rooms, disconnect


from engineio.payload import Payload

Payload.max_decode_packets = 50


app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret key!'
socketio = SocketIO(app)

UPLOAD_DIR = os.path.join('app', 'static', 'uploads')
model = Model(lang="en-us", model_path=os.path.join('model', 'vosk-model-small-en-us-0.15'))


sample_rate = 16000
show_words = True
max_alternatives = 0


@app.route("/")
def index():
    return render_template("index.html")


@socketio.on('connect', namespace='/test')
def test_connect():
    with app.app_context():
        session['room'] = str(uuid.uuid4())

        # time.sleep()

        rec = KaldiRecognizer(model, sample_rate)
        rec.SetWords(show_words)
        rec.SetMaxAlternatives(max_alternatives)

        session['rec'] = rec

        join_room(session['room'])


@socketio.on('my_event', namespace='/test')
def test_message(message):
    emit('my_response', {'data': 'Connection Established ACK', 'count': 0})


from gtts import gTTS
from io import BytesIO


@socketio.on('audio', namespace='/test')
def handle_my_custom_event(audio):
    with app.app_context():
        room = session['room']
        rec = session['rec']

        if rec.AcceptWaveform(audio):
            response_text = rec.Result()
        else:
            response_text = rec.PartialResult()

        import json
        text = json.loads(response_text).get('text')
        mp3_fp = BytesIO()
        tts = gTTS(text, lang='en')
        tts.write_to_fp(mp3_fp)
        tts.save('app/static/a.mp3')
        x = mp3_fp.getvalue()
        if text:
            emit('my_response', {'data': text, 'audio': x}, room=room)


@socketio.on('disconnect_request', namespace='/test')
def test_disconnect():
    with app.app_context():
        session['rec'] = None
    print('Client disconnected')
