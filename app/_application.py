import os
import time
import uuid

from app.__transcribe import recognize

from vosk import Model
from flask import Flask, render_template, request, session
from flask_socketio import SocketIO, emit, join_room
from threading import Thread, Event


app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret key!'
socketio = SocketIO(app, logger=True, engineio_logger=True)

UPLOAD_DIR = os.path.join('app', 'static', 'uploads')
model = Model(lang="en-us", model_path=os.path.join('model', 'vosk-model-en-us-0.22'))

thread = Thread()
thread_stop_event = Event()


class ProgressThread(Thread):
    def __init__(self):
        self.delay = 1
        self._status = 'status'
        self._result = ''
        self.path = ''
        self.sleep = 0.01
        self.room = str(uuid.uuid4())

        join_room(self.room)

        super(ProgressThread, self).__init__()

    def test_function(self):
        self.status = 'started'
        recognize(model, self.path, self)
        self.status = 'completed'

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, value):
        self._status = value
        time.sleep(self.sleep)
        socketio.emit('status', {'status': self._status}, namespace='/progress', room=self.room)

    @property
    def result(self):
        return self._result

    @result.setter
    def result(self, value):
        self._result = value
        time.sleep(self.sleep)
        socketio.emit('result', {'result': self._result}, namespace='/progress', room=self.room)

    def run(self):
        self.test_function()


@app.route("/")
def index():
    return render_template("_index.html")


@app.route('/stream')
def stream():
    return render_template('stream.html')


@socketio.on('start', namespace='/progress')
def start(path):
    global thread

    print(path)
    thread.path = path
    thread.start()


@socketio.on('connect', namespace='/progress')
def test_connect():
    global thread

    print("Starting ProgressThread")
    thread = ProgressThread()


@app.route('/upload', methods=('POST',))
def upload():
    data = request.form

    lang = data.get('language')
    display = data.get('display')

    files = request.files

    audio = files.get('audio')
    if not audio:
        return {
            'status': 'error',
            'path': None
        }

    audio_format = audio.filename.split('.')[-1]
    filename = str(uuid.uuid4()) + '.' + audio_format
    path = os.path.join(UPLOAD_DIR, filename)
    audio.save(path)

    return {
        'status': 'success',
        'path': path
    }
