      var namespace = '/test';
      var socket = null;
      var mediaStream = null;

      document.getElementById('trans').value = '';
      // prepare button state
      $('#disconnect input')[0].disabled = true;

      // audio functions
      function initializeRecorder(stream){
         mediaStream = stream;

         // get sample rate
         audio_context = new AudioContext({sampleRate: 16000});
         sampleRate = audio_context.sampleRate;

         var audioInput = audio_context.createMediaStreamSource(stream);

         console.log("Created media stream.");

         var bufferSize = 4096;
         // record only 1 channel
         var recorder = audio_context.createScriptProcessor(bufferSize, 1, 1);
         // specify the processing function
         recorder.onaudioprocess = recorderProcess;
         // connect stream to our recorder
         audioInput.connect(recorder);
         // connect our recorder to the previous destination
         recorder.connect(audio_context.destination);          
      }
      function recorderProcess(e) {
        var left = e.inputBuffer.getChannelData(0);
        var left16 = convertFloat32ToInt16(left)
        console.log(left16);
        var now = new Date().getTime() / 1000;
        if (player_finish_time === null || now > player_finish_time) {
        socket.emit('audio', left16);
        }
	console.log(player_finish_time);
      }
      function convertFloat32ToInt16(buffer) {
        l = buffer.length;
        buf = new Int16Array(l);
        while (l--) {
          buf[l] = Math.min(1, buffer[l])*0x7FFF;
        }
        return buf.buffer;
      }

      $('form#connect').submit(function(event) {
          if(socket == null){
                console.log(location.protocol + '//' + document.domain + ':' + location.port + namespace);
                socket = io.connect(location.protocol + '//' + document.domain + ':' + location.port + namespace);
                console.log(socket);
                socket.on('connect', function() {
                    socket.emit('my_event', {data: 'I\'m connected!'});
                    navigator.getUserMedia({audio: true}, initializeRecorder, function(a, b, c){
                      console.log(a, b, c);
                    });
                  });
              socket.on('my_response', function(msg) {
                console.log(msg);
                document.getElementById('trans').value = document.getElementById('trans').value + '\n' + msg.data;
                playByteArray(msg.audio)
              });
          } else {
              socket.disconnect();
              socket.connect();
          }
          $('#connect input')[0].disabled = true;
          $('#disconnect input')[0].disabled = false;
          return false;
      });

      $('form#disconnect').submit(function(event) {
          mediaStream.getAudioTracks()[0].stop();
          audio_context.close();
          socket.emit('disconnect_request');
          setTimeout(function(){ console.log('Disconnect request'); }, 500);
          socket.disconnect();
          console.log('Socket disconnected')
          $('#connect input')[0].disabled = false;
          $('#disconnect input')[0].disabled = true;
          return false;
      });
