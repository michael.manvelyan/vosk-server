window.onload = init;
var context;    // Audio context
var buf;        // Audio buffer
var player_finish_time = null;

function init() {
if (!window.AudioContext) {
    if (!window.webkitAudioContext) {
        alert("Your browser does not support any AudioContext and cannot play back this audio.");
        return;
    }
        window.AudioContext = window.webkitAudioContext;
    }

    context = new AudioContext({sampleRate: 32000});
}
function playByteArray( bytes ) {
    var buffer = new Uint8Array( bytes.byteLength);
    buffer.set( new Uint8Array(bytes), 0 );
    context.decodeAudioData(buffer.buffer, play);
}

function play( audioBuffer ) {
    player_finish_time = (new Date().getTime() / 1000) + audioBuffer.duration + 0.2; // 0.2 - delay
    var source = context.createBufferSource();
    source.buffer = audioBuffer;
    source.connect( context.destination );
    source.start(0);
}
