$(document).ready(function(){
    const $input = $('input[type="file"]');
    const $form = $('form');
    const $transcribe = $('#transcribe-button');

    let path = null;

    $input.on('change', function() {
        var formData = new FormData();
        formData.append('audio', $input[0].files[0]);

        $.ajax({
            url: '/upload',
            data: formData,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function(msg) {
                console.log(msg);
                path = msg.path;
            }
        });
    });

    $transcribe.on('click', function() {
        if (path) {
            const socket = io('http://' + document.domain + ':' + location.port + '/progress');
            const $statusBar = $('#status-bar');
            const $resultArea = $('#floatingTextarea');
            $resultArea.val('');
            let text = $resultArea.val()

            socket.emit('start', path);

            socket.on('status', function(msg) {
                console.log(msg)
                let status = msg.status
                $statusBar.val(status)

                if (status === 'completed') {
                    socket.close();
                }
            });

            socket.on('result', function(msg) {
                let result = msg.result
                if (result.length) {
                    console.log(msg)
                    text += result + '\n'
                    $resultArea.val(text)
                }
            });
        }
    })
});
