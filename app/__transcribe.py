import json
import logging
import datetime
import scipy.io.wavfile
import numpy as np
import sys
from vosk import KaldiRecognizer


def process_response_text(in_text, channel_idx):
    out_text = ''
    if in_text != '':
        json_response = json.loads(in_text)
        min_start = sys.maxsize
        max_end = 0
        if 'result' in json_response and 'text' in json_response:
            for res_entry in json_response['result']:
                min_start = min(min_start, res_entry['start'])
                max_end = max(max_end, res_entry['end'])
            out_text = str(datetime.timedelta(seconds=int(min_start))) + ' - ' + str(
                datetime.timedelta(seconds=int(max_end))) + ' <= ' + str(channel_idx) + '=> ' + json_response['text']
    return out_text


def process_chunk(rec, message, run_flag, channel_idx):
    response_text = ''
    if not run_flag:
        response_text = rec.FinalResult()
    elif rec.AcceptWaveform(message):
        response_text = rec.Result()
    else:
        response_text = rec.PartialResult()
    response_text = process_response_text(response_text, channel_idx)
    print(response_text)
    return response_text


def recognize(rec, data):
    if rec.AcceptWaveform(data):
        response_text = rec.Result()
    else:
        response_text = rec.PartialResult()
    response_text = process_response_text(response_text, 0)

    return response_text
